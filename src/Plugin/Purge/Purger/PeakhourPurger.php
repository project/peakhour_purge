<?php

namespace Drupal\peakhour_purge\Plugin\Purge\Purger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\peakhour_purge\Api;
use Drupal\peakhour_purge\Hash;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Peakhour purger.
 *
 * @PurgePurger(
 *   id = "Peakhour",
 *   label = @Translation("Peakhour"),
 *   description = @Translation("Purger for Peakhour."),
 *   configform = "\Drupal\peakhour_purge\Form\PeakhourPurgeConfigForm",
 *   cooldown_time = 0.0,
 *   types = {"tag", "everything", "path"},
 *   multi_instance = FALSE,
 * )
 */
class PeakhourPurger extends PurgerBase implements PurgerInterface {

  /**
   * Peakhour API.
   *
   * @var \Drupal\peakhour_purge\Api
   */
  protected $api;

  /**
   * The settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('peakhour_purge.api')
    // $container->get('Peakhour.cache_tags.hash')
    );
  }

  /**
   * Constructs a \Drupal\Component\Plugin\PeakhourPurger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The factory for configuration objects.
   * @param \Drupal\peakhour_purge\Api $api
   *   Api class for sending purge events to Peakhour.
   *
   * @throws \LogicException
   *   Thrown if $configuration['id'] is missing, see Purger\Service::createId.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config, Api $api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config->get('peakhour_purge.settings');
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'tag'  => 'invalidateTags',
      'path'  => 'invalidatePaths',
      'everything' => 'invalidateAll',
    ];
    return $methods[$type] ?? 'invalidate';
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    throw new \LogicException('This should not execute.');
  }

  /**
   * Invalidate a set of paths.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidatePaths(array $invalidations) {
    $paths = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $paths[] = $invalidation->getExpression();
    }

    if (empty($paths)) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
        throw new \Exception('No path found to purge');
      }
    }

    $result = $this->api->purgeUrls($paths);
    if ($result['success']) {
      $this->updateState($invalidations, InvalidationInterface::SUCCEEDED);
    }
    else {
      $this->updateState($invalidations, InvalidationInterface::FAILED);
    }

  }

  /**
   * Invalidate a set of tags.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateTags(array $invalidations) {
    $tags = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $tags[] = $invalidation->getExpression();
    }

    if (empty($tags)) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
        throw new \Exception('No tag found to purge');
      }
    }

    $result = $this->api->purgeTags(Hash::cacheTags($tags));

    if ($result['success']) {
      $this->updateState($invalidations, InvalidationInterface::SUCCEEDED);
    }
    else {
      $this->updateState($invalidations, InvalidationInterface::FAILED);
    }
  }

  /**
   * Invalidate everything.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   */
  public function invalidateAll(array $invalidations) {
    $this->updateState($invalidations, InvalidationInterface::PROCESSING);
    // Invalidate and update the item state.
    $result = $this->api->purgeAllPublic();
    if ($result['success']) {
      $this->updateState($invalidations, InvalidationInterface::SUCCEEDED);
    }
    else {
      $this->updateState($invalidations, InvalidationInterface::FAILED);
    }
  }

  /**
   * Update the invalidation state of items.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   * @param int $invalidation_state
   *   The invalidation state.
   */
  protected function updateState(array $invalidations, $invalidation_state) {
    // Update the state.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

}
