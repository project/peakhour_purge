Peakhour Purge
===============================

Peakhour is an Australian owned and based global website acceleration and 
security service.

Our service includes in one bundled fee:
* Advanced Global CDN with features like vary on cookie, flush by tag, instant 
  invalidation
* In process, low latency WAF with multiple rulesets and custom rules
* Rate limiting
* Threat Intelligence
* Edge Rules
* Multi origin and load balancing
* Content rewriting
* Edge scripting
* Origin shielding and cache collapsing
* Automatic image optimisation and format conversion, including AVIF and 
  JXL support
* Image manipulation API
* Automatic free lets encrypt SSL certificate
* Advanced BOT management

Based in Sydney, Peakhour makes sure you get the most from our service with
our renowned onboarding and ongoing support.

INSTALLATION
==============================
This module requires a Peakhour account to use. If you don't have one already
you can sign up for a free, no obligation trial at: 

https://www.peakhour.io/app/signup

1. Install the module:

The Peakhour Purge module requires the Drupal Purge module, make sure you install 
the first using composer:

composer require drupal/purge

1.1 To install via composer

composer require drupal/peakhour_purge

1.2 To install manually unzip the module under the Drupal /modules/contrib directory

2. Enable the modules. 

In the drupal admin go to the "Extend" tab and scroll down to the 'PURGE' section. 
Tick all the items under the Purge heading. 

Then, under 'PURGE - PROCESSORS' choose how you want cache invalidation to be 
performed, using a cron job, or for every request. 

Then under 'PURGE - QUEUERS', tick 'Core tags queuer'

Finally under 'PURGE - REVERSE PROXIES & CDNS' tick 'Peakhour Purge'

Then scroll to the bottom and press 'Install'

3. Configure the module

Click on 'Configuration' in the top level menu, then on 'Performance' under the 
'Development' section. On the resulting page choose 1 year from the drop down
under 'Browser and proxy cache maximum age' and click 'Save Configuration'

Then click on the 'Purge' tab. Then click on the 'Add Purger' button. Peakhour
should be the only option, but if you do have multiple Purge modules installed 
then select Peakhour and click 'Add'.

Then click on the down arror next to Peakhour and click 'Configure', a pop up 
will be displayed with the following fields:

'Enabled'
'Your Domain Name' = The domain name as entered in your Peakhour account.
'API Key' = Your API Key, available under your Peakhour account -> API Keys
'API URL' = The API endpoint for Peakhour, should default to 
            https://www.peakhour.io/api/v1/

Fill in these fields (including ticking Enabled) and click 'Save Configuration' 


Congratulations! You've successfully installed and configured the module. Drupal
will add a Cache Control header which will enable Peakhour to automatically
start caching full pages. When content is changed Drupal will issue a flush 
request(s) with the list of tags that have changed. You can view the flush 
requests under your Peakhour account. Log in, administer your domain, then 
click on the 'Flush Logs' left menu item.

Enjoy your website running faster and safer.
