<?php

namespace Drupal\peakhour_purge\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;

/**
 * Provides a config form for the Peakhour purger.
 */
class PeakhourPurgeConfigForm extends PurgerConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['peakhour_purge.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'peakhour_purge.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('peakhour_purge.settings');
    if (empty($settings->get('api_url'))) {
      $settings->set('api_url', 'https://www.peakhour.io/api/v1/');
    }

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Is Peakhour enabled?.'),
      '#default_value' => !empty($settings->get('enabled')),
    ];
    $form['domain_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Domain Name'),
      '#default_value' => $settings->get('domain_name'),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $settings->get('api_key'),
    ];
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $settings->get('api_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('peakhour_purge.settings');
    $settings->set('enabled', $form_state->getValue('enabled'));
    $settings->set('domain_name', $form_state->getValue('domain_name'));
    $settings->set('api_key', $form_state->getValue('api_key'));
    $settings->set('api_url', $form_state->getValue('api_url'));
    $settings->save();
  }

}
