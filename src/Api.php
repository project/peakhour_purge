<?php

namespace Drupal\peakhour_purge;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Peakhour Api for Drupal.
 */
class Api {

  const FLUSH_ALL = 'all';

  /**
   * Base url for the Peakhour API.
   *
   * @var string
   */
  protected $peakhourUrl;

  /**
   * API Key for authenticating purge requests to Peakhour.
   *
   * Grab/create your key from your Peakhour account.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Your Domain Name, as used on your Peakhour service.
   *
   * @var string
   */
  protected $domain;

  /**
   * The Peakhour logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Guzzle http client for sending requests.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Peakhour logger channel.
   *
   * @var bool
   */
  protected $enabled;

  /**
   * Connect timeout.
   *
   * @var string
   */
  protected $connectTimeout;

  /**
   * Construct the Api class and set the settings.
   */
  public function __construct(ConfigFactoryInterface $config_factory, $connect_timeout, ClientInterface $http_client, LoggerInterface $logger) {

    $config = $config_factory->get('peakhour_purge.settings');
    $this->apiKey = $config->get('api_key');
    $this->domain = $config->get('domain_name');
    $this->peakhourUrl = $config->get('api_url');
    $this->enabled = $config->get('enabled');
    $this->httpClient = $http_client;
    $this->connectTimeout = $connect_timeout;
    $this->logger = $logger;
  }

  /**
   * @return string
   */
  public function getPeakhourUrl(): string {
    return $this->peakhourUrl;
  }

  /**
   * @param string $peakhourUrl
   */
  public function setPeakhourUrl(string $peakhourUrl): void {
    $this->peakhourUrl = $peakhourUrl;
  }

  /**
   * @return string
   */
  public function getApiKey(): string {
    return $this->apiKey;
  }

  /**
   * @param string $apiKey
   */
  public function setApiKey(string $apiKey): void {
    $this->apiKey = $apiKey;
  }

  /**
   * @return string
   */
  public function getDomain(): string {
    return $this->domain;
  }

  /**
   * @param string $domain
   */
  public function setDomain(string $domain): void {
    $this->domain = $domain;
  }

  /**
   * @return \Psr\Log\LoggerInterface
   */
  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function setLogger(LoggerInterface $logger): void {
    $this->logger = $logger;
  }

  /**
   * @return \GuzzleHttp\ClientInterface
   */
  public function getHttpClient(): ClientInterface {
    return $this->httpClient;
  }

  /**
   * @param \GuzzleHttp\ClientInterface $httpClient
   */
  public function setHttpClient(ClientInterface $httpClient): void {
    $this->httpClient = $httpClient;
  }

  /**
   * @return bool
   */
  public function isEnabled(): bool {
    return $this->enabled;
  }

  /**
   * @param bool $enabled
   */
  public function setEnabled(bool $enabled): void {
    $this->enabled = $enabled;
  }

  /**
   * Purge all public cache of this site.
   *
   * @since 1.0.0
   */
  public function purgeAllPublic() {
    $resourcesUrl = $this->getResourcesUrl();
    $body = $this->makeRequestBody([], self::FLUSH_ALL, TRUE);
    return $this->doRequest($resourcesUrl, 'DELETE', $body);
  }

  /**
   * Purge Peakhour Cache using the input tags.
   */
  public function purgeTags(array $tags) {
    $this->logger->debug("flushing: " . implode(",", $tags));
    $response = $this->doRequest($this->getTagsUrl(), 'DELETE', json_encode(['tags' => $tags]));
    return $response;
  }

  /**
   * Purge Peakhour Cache using the input paths, eg ['/about/','/contact/'].
   */
  public function purgeUrls(array $paths) {
    $resourcesUrl = $this->getResourcesUrl();
    $body = $this->makeRequestBody($paths);
    return $this->doRequest($resourcesUrl, 'DELETE', $body);
  }

  /**
   * Send a test request to Peakhour to test configuration.
   */
  public function testConnection() {
    $resourcesUrl = $this->getResourcesUrl();
    return $this->doRequest($resourcesUrl, 'OPTIONS', NULL);
  }

  /**
   * Build the body of the request to Peakhour.
   */
  public function makeRequestBody($urls, $flushType = self::FLUSH_ALL, $purgeAll = FALSE) {
    $paths = [];

    foreach ($urls as $url) {
      $parsed = parse_url($url);
      $path = ($parsed['path'] ?? NULL);
      if (empty($path)) {
        $path = '/';
      }
      if (isset($parsed['query'])) {
        $path = $path . '?' . $parsed['query'];
      }
      array_push($paths, $path);

    }
    $paths = array_values(array_unique($paths));

    return json_encode([
      'paths' => $paths,
      'flush_type' => $flushType,
      'purge_all' => $purgeAll,
    ]);
  }

  /**
   * Get the Url for purging by tags.
   */
  public function getTagsUrl(): string {
    if (is_null($this->domain) || is_null($this->peakhourUrl)) {
      return "";
    }
    return $this->peakhourUrl . 'domains/' . $this->domain . '/services/rp/cdn/tag';
  }

  /**
   * Get the Url for purging by path.
   */
  public function getResourcesUrl($domain = NULL): string {
    if (is_null($this->domain) || is_null($this->peakhourUrl)) {
      return "";
    }
    return $this->peakhourUrl . 'domains/' . $this->domain . '/services/rp/cdn/resources';
  }

  /**
   * Make the request to Peakhour.
   */
  public function doRequest($url, $method, $body = NULL) {
    if (is_null($this->apiKey) || is_null($this->domain) || is_null($this->peakhourUrl)) {
      return [
        'success' => FALSE,
        'error' => "You need to make sure you've provided your API Key, peakhour api url, and domain in the Peakhour module settings",
      ];
    }
    $headers = [
      'Accept' => 'application/json',
      'Authorization' => 'Bearer ' . $this->apiKey,
      'Content-Type' => 'application/json',
    ];

    try {

      if (!is_null($body)) {
        $response = $this->httpClient->request(
          $method,
          $url,
          [
            'headers' => $headers,
            'body' => $body,
            'connect_timeout' => $this->connectTimeout,
          ]
        );
      }
      else {
        $response = $this->httpClient->request(
          $method,
          $url,
          [
            'headers' => $headers,
            'connect_timeout' => $this->connectTimeout,
          ]
        );
      }
      $this->logger->debug("returning");
      return [
        'success' => TRUE,
        'error' => NULL,
        'body' => $response->getBody(),
      ];
    }
    catch (\Exception $e) {
      $this->logger->debug("Failed $method to $url: " . $e->getMessage());
      return ['success' => FALSE, 'error' => $e->getMessage()];
    }
  }

}
