<?php

namespace Drupal\peakhour_purge\Plugin\Purge\TagsHeader;

use Drupal\peakhour_purge\Hash;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderInterface;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderBase;

/**
 * Sets and formats the default response header with cache tags.
 *
 * @PurgeTagsHeader(
 *   id = "peakhour_tagsheader",
 *   header_name = "X-Drupal-Tags",
 * )
 */
class PeakhourCacheTagsHeader extends TagsHeaderBase implements TagsHeaderInterface {

  /**
   * {@inheritdoc}
   */
  public function getValue(array $tags) {
    return new CacheTagsHeaderValue($tags, Hash::cacheTags($tags));
  }

}
